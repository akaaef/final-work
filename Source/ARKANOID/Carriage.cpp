// Fill out your copyright notice in the Description page of Project Settings.


#include "Carriage.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ACarriage::ACarriage()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PawnMove = CreateDefaultSubobject<UFloatingPawnMovement>("PawnMove");
}

// Called when the game starts or when spawned
void ACarriage::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ACarriage::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ACarriage::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("MoveLeftRight", this, &ACarriage::MoveLeftRight);
}

void ACarriage::MoveLeftRight(float value)
{	
	float speed = 1250.f;
	if (IsValid(GetWorld()))
	{
		AddActorLocalOffset(FVector(value * speed * UGameplayStatics::GetWorldDeltaSeconds(GetWorld()) * -1, 0.f, 0.f), true);
	}
}



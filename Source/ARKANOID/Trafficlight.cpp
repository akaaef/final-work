// Fill out your copyright notice in the Description page of Project Settings.


#include "Trafficlight.h"

// Sets default values
ATrafficlight::ATrafficlight()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATrafficlight::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATrafficlight::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


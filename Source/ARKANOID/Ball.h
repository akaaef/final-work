// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TimerManager.h"
#include "Ball.generated.h"


UCLASS()
class ARKANOID_API ABall : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABall();
	FVector Move;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Options")
	float Speed = 9.0;
	FVector Direction = FVector(1, 0, 0);
	FTimerHandle Handle;
	float Block = FMath::RandRange(10,20);
	float Ignore = FMath::RandRange(10,20);
	UPROPERTY(EditAnyWhere)
	USoundBase* MySound;
	UPROPERTY(EditAnyWhere)
	UAudioComponent* MyAudioComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void NotifyHit(UPrimitiveComponent* MyComp, AActor* Other, UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit);
	virtual void SetCollisionIgnore();
	virtual void SetCollisionBlock();
	void Speedup();
	void Speeddown();
	void RandomY();
};
// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ARKANOIDGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class ARKANOID_API AARKANOIDGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};

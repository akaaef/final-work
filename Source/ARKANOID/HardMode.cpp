// Fill out your copyright notice in the Description page of Project Settings.


#include "HardMode.h"
#include <Runtime/Engine/Classes/Kismet/KismetMathLibrary.h>
#include "GameFramework/FloatingPawnMovement.h"

// Sets default values
AHardMode::AHardMode()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AHardMode::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AHardMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move = (Direction * Speed) * DeltaTime;
	AddActorWorldOffset(Move, true);

}

void AHardMode::NotifyHit(UPrimitiveComponent* MyComp, AActor* Other, UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit)
{	
	Super::NotifyHit(MyComp, Other, OtherComp, bSelfMoved, HitLocation, HitNormal, NormalImpulse, Hit);

	ABall* HitBall = Cast<ABall>(Hit.Actor);
	if (!HitBall)
	{
		FVector ReflectedDirection = UKismetMathLibrary::GetReflectionVector(Direction, HitNormal);
		Direction = ReflectedDirection;
	}
}

void AHardMode::SetCollision()
{
	UPrimitiveComponent* CollisionComponent = FindComponentByClass<UPrimitiveComponent>();
	CollisionComponent->SetCollisionResponseToChannel(ECC_Pawn,ECR_Block);
}


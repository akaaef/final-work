// Fill out your copyright notice in the Description page of Project Settings.


#include "Ball.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/PrimitiveComponent.h"
#include "Components/AudioComponent.h"


// Sets default values
ABall::ABall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ABall::BeginPlay()
{
	Super::BeginPlay();
    RandomY();
    GetWorldTimerManager().SetTimer(Handle, this, &ABall::SetCollisionIgnore,Ignore,false); // ������ �� �������� � ��������� ����
}
	

// Called every frame
void ABall::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    Move = (Direction * Speed) * DeltaTime;
    AddActorWorldOffset(Move, true);
}

void ABall::NotifyHit(UPrimitiveComponent* MyComp, AActor* Other, UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit)
{
    Super::NotifyHit(MyComp, Other, OtherComp, bSelfMoved, HitLocation, HitNormal, NormalImpulse, Hit);
    FVector ReflectedDirection = UKismetMathLibrary::GetReflectionVector(Direction, HitNormal);
    Direction = ReflectedDirection;
}

void ABall::SetCollisionIgnore()
{
    UPrimitiveComponent* BallCollisionComponent = FindComponentByClass<UPrimitiveComponent>();
    BallCollisionComponent->SetCollisionResponseToChannel(ECC_WorldStatic, ECR_Ignore);
    Speedup();
    GetWorldTimerManager().SetTimer(Handle, this, &ABall::SetCollisionBlock,Block,false);
}

void ABall::SetCollisionBlock()
{
    UPrimitiveComponent* BallCollisionComponent = FindComponentByClass<UPrimitiveComponent>();
    BallCollisionComponent->SetCollisionResponseToChannel(ECC_WorldStatic, ECR_Block);
    Speeddown();
}

void ABall::Speedup()
{
    Speed = 13;
}

void ABall::Speeddown()
{
    Speed = 9;
}

void ABall::RandomY() // ������ ����� � ������ ���������� ������ ��� �����
{
    int32 RandomValue = FMath::RandRange(0, 1);
    if (RandomValue == 0)
    {
        FVector Vector = FVector(100.0, 100.f, 0);
        Direction = Vector;
    }
    else
    {
        FVector Vector = FVector(100.0, -100.f, 0);
        Direction = Vector;
    }
}


// Fill out your copyright notice in the Description page of Project Settings.


#include "SpawnLevel.h"

// Sets default values
ASpawnLevel::ASpawnLevel()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ASpawnLevel::BeginPlay()
{
	Super::BeginPlay();
    GenerateBlocks();
}

// Called every frame
void ASpawnLevel::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASpawnLevel::GenerateBlocks()
{
    // ��������� ������ �� ������/�������� ����� � ��������
    for (int32 Row = 0; Row < Rows; Row++)
    {
        for (int32 Col = 0; Col < Columns; Col++)
        {
            if ((Row % 2 == 0 && Col % 2 == 0) || (Row % 2 != 0 && Col % 2 != 0))
            {
                FVector SpawnLocation = FVector(Row * 70.f, Col * 70.f,0.f);
                FRotator SpawnRotation = FRotator::ZeroRotator;
                FActorSpawnParameters SpawnParams;
                GetWorld()->SpawnActor<AActor>(BlockTemplate, SpawnLocation, SpawnRotation, SpawnParams);
                SpawnBlocks++;
            }
        }
    }
}


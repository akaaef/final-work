// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SpawnLevel.generated.h"

UCLASS()
class ARKANOID_API ASpawnLevel : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawnLevel();
	int Rows = FMath::RandRange(5, 9);
	int Columns = FMath::RandRange(9, 13);
	FTimerHandle Handle;
	UPROPERTY(BlueprintReadWrite)
	int SpawnBlocks;
	UPROPERTY(EditAnywhere,Category = "Block Generation")
	TSubclassOf<AActor> BlockTemplate;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;
	void GenerateBlocks();
};
